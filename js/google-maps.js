/*global google */

// マップの中心の緯度経度
var center = { lat: 40.4774912, lng: 141.5602176 };

// マーカーを追加する位置
var places = [
    {
        title: "八戸工業大学 システム情報工学専門棟",
        position: { lat: 40.479254, lng: 141.561181 }
    },
    {
        title: "ミニストップ 八戸工業大学前店",
        position: { lat: 40.482106, lng: 141.561742 }
    }
];

function initMap() {
    // idがmapの要素を取得
    var element = document.getElementById('map');

    // マップ・オブジェクト
    var map = new google.maps.Map(element, {
        center: center,
        zoom: 15 // ズーム
    });

    // マーカーの追加
    for (var i = 0; i < places.length; i = i + 1) {
        var marker = new google.maps.Marker({
            title: places[i].title,
            position: places[i].position,
            map: map
        });
    }
}
